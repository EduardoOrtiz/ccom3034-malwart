/*
 *  Mal_Wart.h
 *  
 *
 *  Created by Computer Science Student on 10/25/13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

#include <iostream>
#include <set>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	int days = 1;
	int amount_of_bills;
	double bill;
	
	vector<double> DayAmount;
	
	srand(time(NULL));
	
	cout << "Let's initiate the Mal Wart statergy. " << endl;

	while( days != 0 )
	{
		multiset<double> Bills;
		double partial_sum = 0.0;
		
		cout << "Insert the amount of days: \t";
	
		cin >> days;

		cout << endl;

		if(days <= 0) break;

		vector<double> Summation;
		
		for(int c = 0; c < days; c++)
		{
			vector<double> Display_V;
			
			cout << "Amount of bills for day " << c + 1 << "? \t";

			cin >> amount_of_bills;

			if(amount_of_bills < 0)
			{
				cout << "Less than 0 bills?" << endl;
				break;
			}

			for(int i = 0; i < amount_of_bills; i++)
			{
				cin >> bill;
				Bills.insert((double)bill);
				Display_V.push_back((double)bill);
			}
				
			
			cout << endl << endl << "For day " 
				 << c + 1 << " the bills are: " << endl;
			
			for(int i = 0; i < Display_V.size(); i++)
				cout << Display_V[i] << " ";
			
			cout << endl << endl;
			
			if(Bills.size() > 0)
			{
				Summation.push_back(*(Bills.rbegin()) - *(Bills.begin()));
			
				//removes the max and min
				Bills.erase(Bills.find(*(Bills.rbegin())));
				
				if(Bills.size() > 0)
					Bills.erase(Bills.find(*(Bills.begin())));
			}
		}
		
		for(int k = 0; k < Summation.size(); k++)
			partial_sum += Summation[k];
		
		DayAmount.push_back(partial_sum);
	}
	
	for(int k = 0; k < DayAmount.size(); k++)
		cout << DayAmount[k] << endl;
	
	return 0;
	
}
			